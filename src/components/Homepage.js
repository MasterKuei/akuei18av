import React, { useState, useCallback, useRef, useMemo } from "react"
import Player from "./Player"
import useMount from "../hooks/useMount"
import useAd from "../hooks/useAd"
import logo from "../images/logo.svg"
import search from "../images/search.svg"
import "./Homepage.scss"

export default function Homepage() {
  const [videoList, setVideoList] = useState([])
  const [vid, setVid] = useState(null)
  const [searchKeyword, setSearchKeyword] = useState("")
  const { showAd, openAd } = useAd(["http://www.google.com.tw", "https://reactjs.org/", "http://www.skylinetw.com/"])
  const searchInputEl = useRef(null)

  const thumbnailClickHandler = useCallback(
    id => {
      if (showAd) {
        openAd()
      } else {
        setVid(id)
      }
    },
    [showAd]
  )

  useMount(() => {
    fetch("json/getVideoList.json")
      .then(response => {
        return response.json()
      })
      .then(json => {
        setVideoList(json.data)
      })
  })

  const displayList = useMemo(() => videoList.filter(el => el.name.includes(searchKeyword)), [videoList, searchKeyword])

  return (
    <>
      <header className="Header">
        <div className="Header-Container">
          <img className="Header-Logo" src={logo} alt="" />
          <div className="Header-Search">
            <input className="Header-Input" ref={searchInputEl} type="text" placeholder="搜尋影片標題" />
            <button
              className="Header-Btn"
              onClick={() => {
                setSearchKeyword(searchInputEl.current.value)
              }}
            >
              <img className="Header-Icon" src={search} alt="" />
            </button>
          </div>
        </div>
      </header>
      <div className="Homepage">
        <div className="Homepage-Container">
          <div className="Homepage-VideoList">
            {displayList.length > 0 ? (
              displayList.map(el => (
                <div key={el.id} className="Homepage-VideoItem">
                  <img
                    className="Homepage-VideoThumbnail"
                    src={el.thumbnal}
                    alt="thumbnail"
                    onClick={() => {
                      thumbnailClickHandler(el.id)
                    }}
                  />
                  <p className="Homepage-VideoName">{el.name}</p>
                  <p className="Homepage-VideoActor">{el.actor}</p>
                </div>
              ))
            ) : (
              <p>暫無數據</p>
            )}
          </div>
        </div>

        {vid !== null && (
          <Player
            vid={vid}
            close={() => {
              setVid(null)
            }}
          />
        )}
      </div>
    </>
  )
}
