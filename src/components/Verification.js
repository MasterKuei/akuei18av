import React, { useState, useMemo, useCallback } from "react"
import useWindowSize from "../hooks/useWindowSize"
import "./Verification.scss"

export default function Verification() {
  const [visible, setVisible] = useState(true)
  const [age, setAge] = useState(18)
  const isAdult = useMemo(() => age >= 18, [age])
  const windowSize = useWindowSize()

  const clickHandler = useCallback(() => {
    if (isAdult) {
      setVisible(false)
    } else {
      window.location = "https://disney.com.tw/"
    }
  }, [isAdult])

  if (!visible) {
    return null
  }

  return (
    <div className="Verification">
      <div className="Verification-Wrapper">
        <p className="Verification-Text">本站為限制級網站，含有許多成人內容，您必須年滿十八歲才可以瀏覽本站內容！</p>
        <div className="Verification-Age">
          <span>我的真實年齡是</span>
          <input
            className="Verification-Input"
            value={age}
            onChange={e => {
              setAge(e.target.value)
            }}
          />
          <span>歲({windowSize.width})</span>
        </div>

        <button className="Verification-Btn" onClick={clickHandler}>
          進入網站
        </button>
      </div>
    </div>
  )
}
