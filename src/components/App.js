import React, { Component } from "react"
import Verification from "./Verification"
import Homepage from "./Homepage"
import "./App.scss"

class App extends Component {
  render() {
    return (
      <div className="App">
        <Verification />
        <Homepage />
      </div>
    )
  }
}

export default App
