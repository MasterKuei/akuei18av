import React, { useState } from "react"
import useWindowSize from "../hooks/useWindowSize"
import useMount from "../hooks/useMount"
import useAd from "../hooks/useAd"
import "./Player.scss"

export default function Player(props) {
  const [video, setVideo] = useState({})
  const { showAd, openAd } = useAd(["http://www.google.com.tw", "https://reactjs.org/", "http://www.skylinetw.com/"])
  const windowSize = useWindowSize()

  useMount(() => {
    fetch(`json/getVideo.json?id=${props.vid}`)
      .then(response => {
        return response.json()
      })
      .then(json => {
        setVideo(json.data)
      })
  })

  return (
    <div className="Player">
      <div className="Player-Background" />
      <div className="Player-Wrapper">
        <div className="Player-VideoWrapper">
          <iframe
            className="Player-Video"
            title="av"
            width="966"
            height="543"
            src={video.url}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
          {showAd && <div className="Player-Overlay" onClick={openAd} />}
        </div>

        <p className="Player-Name">{video.name}</p>
        <p className="Player-Actor">
          {video.actor}({windowSize.width})
        </p>
      </div>

      <p className="Player-Close" onClick={props.close}>
        關閉
      </p>
    </div>
  )
}
