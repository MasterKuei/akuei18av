import { useState, useRef, useMemo, useCallback, useEffect } from "react"

export default function useAd(urls) {
  // 是否需要顯示廣告
  const [showAd, setShowAd] = useState(true)
  // 廣告顯示次數
  const [count, setCount] = useState(0)
  // 計時器實例
  const timer = useRef(null)
  // 顯示廣告
  const openAd = useCallback(() => {
    if (showAd) {
      window.open(urls[Math.floor(Math.random() * urls.length)])
      setShowAd(false)
      if (count < 2) {
        setCount(count + 1)
        timer.current = setTimeout(() => {
          setShowAd(true)
        }, 5000)
      }
    }
  }, [showAd, count])

  // 退出時清除計時器
  useEffect(() => {
    return () => {
      clearTimeout(timer.current)
    }
  }, [])

  const result = useMemo(() => {
    return { showAd, openAd }
  }, [showAd])

  return result
}
